package clases;

public class Planetas {
	/*aunque tanto la estrella como el id del sistema solar sirven para referirse a otro sistema solar,
	 * he preferido  usar la idSistemaSolar como atributo que se refeire a la otra clase
	 */
	
	private int numeroDeSatelites;
	private int masaPlaneta;
	private String nombre;
	private int idSistemaSolar;

	public Planetas() {
		this.nombre = "Tierra";
		this.numeroDeSatelites = 8;
		this.masaPlaneta = 198000000;
	}


	public Planetas(int numeroDeSatelites, int masaPlaneta, String nombre) {
		this.numeroDeSatelites = numeroDeSatelites;
		this.masaPlaneta = masaPlaneta;
		this.nombre = nombre;
	}
	

	public int getMasaPlaneta() {
		return masaPlaneta;
	}

	public void setMasaPlaneta(int masaPlaneta) {
		this.masaPlaneta = masaPlaneta;
	}


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Planetas(int numeroDeSatelites, String estrella, int masaPlaneta, String nombre) {
		this.numeroDeSatelites = numeroDeSatelites;
		this.nombre = nombre;
		this.masaPlaneta = masaPlaneta;
	}

	
	public int getIdSisetmaSolar() {
		return idSistemaSolar;
	}
	public void setIdSistemaSolar(int idSistemaSolar) {
		this.idSistemaSolar = idSistemaSolar;
	}
	
	public int getNumeroDeSatelites() {
		return numeroDeSatelites;
	}

	public void setNumeroDeSatelites(int numeroDeSatelites) {
		this.numeroDeSatelites = numeroDeSatelites;
	}
	@Override
	public String toString() {
		return "Planetas [numeroDeSatelites=" + numeroDeSatelites + ", masaPlaneta=" + masaPlaneta + ", nombre=" + nombre + ", idSistemaSolar=" + idSistemaSolar + "]";
	}
	
	
}
