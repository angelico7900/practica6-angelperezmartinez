package clases;

import java.util.ArrayList;
import java.util.Iterator;

public class GestorClases {

	ArrayList<Planetas> listaPlanetas;

	ArrayList<SistemaSolar> listaSistemaSolar;

	public GestorClases() {
		this.listaPlanetas = new ArrayList<>();

		this.listaSistemaSolar = new ArrayList<>();
	}

	public void altaSistemaSolar(int id, String estrella, int cantidadPlanetas, double masaEstrella) {
		if (existeSistemaSolar(id) == false) {
			SistemaSolar sistemita = new SistemaSolar(estrella, cantidadPlanetas, masaEstrella, id);
			listaSistemaSolar.add(sistemita);

		} else {
			System.out.println("Ya existe ese sistema solar");
		}

	}

	public void listarSistemaSolar() {
		for (SistemaSolar s : listaSistemaSolar) {

			System.out.println(s.toString());
		}
	}

	public void buscarPorIDSistemaSolar(int id) {
		for (int i = 0; i < listaSistemaSolar.size(); i++) {
			if (listaSistemaSolar.get(i).getId() == id) {
				System.out.println(listaSistemaSolar.get(i).toString());
			}
		}
	}

	public void eliminarSistemaSolar(int id) {

		for (int i = 0; i < listaSistemaSolar.size(); i++) {
			if (listaSistemaSolar.get(i).getId() == id) {
				listaSistemaSolar.remove(i);
				break;
			}
		}
	}

	public void altaPlaneta(int numeroDeSatelites, int masaPlaneta, String nombre, int idSistemaSolar) {
		if (existePlaneta(nombre) == false) {
			Planetas planetita = new Planetas(numeroDeSatelites, masaPlaneta, nombre);
			listaPlanetas.add(planetita);

			AsignarIdSistemaSolarAPlaneta(idSistemaSolar, nombre);
		}
	}

	public void listarPlanetas() {
		Iterator<Planetas> iterador = listaPlanetas.iterator();
		while (iterador.hasNext()) {
			System.out.println(iterador.next().toString());
		}
	}

	public void listarPlanetaPorIdDeSistemaSolar(int idSistemaSolar) {
		for (int i = 0; i < listaPlanetas.size(); i++) {
			if (listaPlanetas.get(i).getIdSisetmaSolar() == idSistemaSolar) {

				System.out.println(listaPlanetas.get(i).toString());
			}

		}
	}

	public void elminarPlaneta(String nombre) {
		for (int i = 0; i < listaPlanetas.size(); i++) {
			if (listaPlanetas.get(i).getNombre().equals(nombre)) {
				listaPlanetas.remove(i);
			}
		}
	}

	// Busca un planeta por el rango de masa indicado
	public void buscarPorRangoDeMasa(int masaMinima, int masaMaxima) {
		for (int i = 0; i < listaPlanetas.size(); i++) {
			if (listaPlanetas.get(i).getMasaPlaneta() >= masaMinima
					&& listaPlanetas.get(i).getMasaPlaneta() <= masaMaxima) {
				System.out.println(listaPlanetas.get(i).toString());
			}
		}

	}

	public void buscarPlanetaPorNombre(String nombre) {
		for (Planetas p : listaPlanetas) {
			if (p != null && p.getNombre().equals(nombre)) {
				System.out.println(p.toString());
			}
		}
	}

	public void AsignarIdSistemaSolarAPlaneta(int idSistemaSolar, String nombre) {

		for (int i = 0; i < listaPlanetas.size(); i++) {
			if (listaPlanetas.get(i).getNombre().equals(nombre)) {
				listaPlanetas.get(i).setIdSistemaSolar(idSistemaSolar);
			}
		}

	}

	public boolean existeSistemaSolar(int id) {
		for (int i = 0; i < listaSistemaSolar.size(); i++) {
			if (listaSistemaSolar.get(i).getId() == id) {
				return true;
			}
		}
		return false;

	}

	public boolean existePlaneta(String nombre) {
		for (Planetas p : listaPlanetas) {
			if (p != null && p.getNombre().equals(nombre)) {
				return true;
			}
		}
		return false;
	}

	// intercambia dos ids entre ellos de los sistemas solares
	public void intercambiarIdSistemasSolares(int id1, int id2) {
		int valor1 = 0;
		int valor2 = 0;
		for (int i = 0; i < listaSistemaSolar.size(); i++) {
			if (listaSistemaSolar.get(i).getId() == id1) {
				valor1 = i;
			}
			if (listaSistemaSolar.get(i).getId() == id2) {
				valor2 = i;
			}
		}
		listaSistemaSolar.get(valor1).setId(id2);
		listaSistemaSolar.get(valor2).setId(id1);
	}

	public void modificarNombreEstrella(String estrella) {
		for (int i = 0; i < listaSistemaSolar.size(); i++) {
			if (listaSistemaSolar.get(i).getEstrella().equals(estrella)) {
				if (listaSistemaSolar.get(i).getCantidadPlanetas() > 5) {
					if (!(listaSistemaSolar.get(i).getEstrella().endsWith("-poblada"))) {
						listaSistemaSolar.get(i).setEstrella(listaSistemaSolar.get(i).getEstrella() + "-poblada");
					}else {
						System.out.println("Ya esta renombrada");
					}
				} else {
					if (!(listaSistemaSolar.get(i).getEstrella().endsWith("-pocoPoblada"))) {
						listaSistemaSolar.get(i).setEstrella(listaSistemaSolar.get(i).getEstrella() + "-pocoPoblada");
					}else {
						System.out.println("ya esta renombrada");
					}
				}
			}
		}
	}

}
