package clases;

public class SistemaSolar {
	private String estrella;
	private int cantidadPlanetas;
	private double masaEstrella;
	private int id;
	
	public SistemaSolar(int id) {
		this.id = id;
	}
	
	public SistemaSolar() {
		this.estrella = "sol";
		this.cantidadPlanetas = 9;
		this.masaEstrella = 12.4;
	}
	
	public SistemaSolar(String estrella, int cantidadPlanetas, double masaEstrella, int id) {
		this.estrella = estrella;
		this.cantidadPlanetas = cantidadPlanetas;
		this.masaEstrella = masaEstrella;
		this.id = id;
	}
	public String getEstrella() {
		return estrella;
	}
	public void setEstrella(String estrella) {
		this.estrella = estrella;
	}
	public int getCantidadPlanetas() {
		return cantidadPlanetas;
	}
	public void setCantidadPlanetas(int cantidadPlanetas) {
		this.cantidadPlanetas = cantidadPlanetas;
	}
	public double getMasaEstrella() {
		return masaEstrella;
	}
	public void setMasaEstrella(double masaEstrella) {
		this.masaEstrella = masaEstrella;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "SistemaSolar [estrella=" + estrella + ", cantidadPlanetas=" + cantidadPlanetas + ", masaEstrella="
				+ masaEstrella + ", id=" + id + "]";
	}
	

	

}
