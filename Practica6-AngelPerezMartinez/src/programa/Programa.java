package programa;


import java.util.Scanner;

import clases.GestorClases;

public class Programa {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		GestorClases planeta = new GestorClases();

		GestorClases sistemaSolar = new GestorClases();

		int opcion = 0;
		// Los creo aqu� por si necesito reutilizarlos y solo crearlos una vez
		int id;
		String nombre;
		String estrella;
		int numeroSatelites;
		double masaEstrella;
		int masaPlaneta;
		int idSistemaSolar;
		int cantidadPlanetas;

		do {

			System.out.println("Se abre el menu");

			System.out.println("Opcion 1- Dar de alta a un Sistema Solar");
			System.out.println("Opcion 2- listar sistemas solares");
			System.out.println("Opcion 3- buscar un sistema solar por su id");
			System.out.println("Opcion 4- eliminar a un sistema solar");
			System.out.println("Opcion 5- Dar de alta un Planeta");
			System.out.println("Opcion 6- listar planetas");
			System.out.println("Opcion 7- Buscar un planeta por su nombre");
			System.out.println("Opcion 8- Listar planetas por rango de masas");
			System.out.println("Opcion 9- Listar planetas por id de del sistema solar");
			System.out.println("Opcion 10- Asignar el id de un sistema solar a un planeta");
			System.out.println("Opcion 11- Eliminar un trabajo");
			System.out.println("Opcion 12- Intercambia dos ids de los sistemas solares");
			System.out.println("Opcion 13- Renombra el nombre de la estrella de un sistema solar en funcion del numero de planetas");
			System.out.println("Opcion 14- Salir");
			System.out.println("Elige una opcion");
			opcion = in.nextInt();

			switch (opcion) {

			case 1:
				for(int i = 0; i < 3; i ++) {
				System.out.println("Introduzca el nombre de la estrella");
				in.nextLine();
				estrella = in.nextLine();
				System.out.println("Introduce la id del sistema solar");
				id = in.nextInt();
				System.out.println("Introduzca la masa de la estrella");
				masaEstrella = in.nextDouble();
				System.out.println("Introduzca la cantidad de planetas");
				cantidadPlanetas = in.nextInt();
				sistemaSolar.altaSistemaSolar(id,estrella,cantidadPlanetas,masaEstrella);
				if(i != 2) {
					System.out.println("Vuleva a introducir los datos");
					}
				}
				sistemaSolar.listarSistemaSolar();
				break;

			case 2:
				sistemaSolar.listarSistemaSolar();
				break;

			case 3:
				System.out.println("Introduce el id por el que desees buscar al sistema solar");
				id = in.nextInt();
				sistemaSolar.buscarPorIDSistemaSolar(id);
				break;

			case 4:
				System.out.println("Introduce el id del sistema solar que desea eliminar");
				id = in.nextInt();
				sistemaSolar.eliminarSistemaSolar(id);
				sistemaSolar.listarSistemaSolar();
				break;

			case 5:
				boolean comprobar;
				for(int i = 0; i < 3; i ++) {
					do {
						comprobar = false;
				System.out.println("Introduce su numero de satelites");
				numeroSatelites = in.nextInt();
				System.out.println("Introduce la masa del planeta");
				masaPlaneta = in.nextInt();
				System.out.println("Introduce el nombre del planeta");
				in.nextLine();
				nombre = in.nextLine();
				System.out.println("Introduce la id del sistema solar al que pertenece");
				idSistemaSolar = in.nextInt();
				if(sistemaSolar.existeSistemaSolar(idSistemaSolar)) {

				planeta.altaPlaneta(numeroSatelites, masaPlaneta, nombre, idSistemaSolar);
				if(i != 2) {
				System.out.println("Vuleva a introducir los datos");
				}
				}else {
					System.out.println("no existe el id del sistema solar por lo que no se ha dado de alta el planeta");
					comprobar = true;
				}
					}while(comprobar);
				}
				break;

			case 6:

				planeta.listarPlanetas();

				break;

			case 7:
				System.out.println("Introduzca el nombre del planeta que desea buscar");
				in.nextLine();
				nombre = in.nextLine();

				planeta.buscarPlanetaPorNombre(nombre);

				break;

			case 8:

				System.out.println("Introduzca el rango de masa minimo por el que desea buscar a los planetas");
				int masaMinima = in.nextInt();
				System.out.println("Introduzca el rango de masa maximo por el que desea buscarlo");
				int masaMaxima = in.nextInt();
				planeta.buscarPorRangoDeMasa(masaMinima, masaMaxima);

				break;
			case 9:
				System.out.println("Introduzca el id del sistema solar para listar dichos planetas");
				idSistemaSolar = in.nextInt();
				planeta.listarPlanetaPorIdDeSistemaSolar(idSistemaSolar);
				break;

			case 10:

				System.out.println("Introduzca el nombre del planeta al que deseas introducir el id de un sistema solar");
				in.nextLine();
				nombre = in.nextLine();
				System.out.println("Introduce el id del sistema solar");
				id = in.nextInt();
				if(sistemaSolar.existeSistemaSolar(id)) {
					planeta.AsignarIdSistemaSolarAPlaneta(id, nombre);
					planeta.listarPlanetas();
				}else {
					System.out.println("esa id no corresponde a ningun sistema solar");
				}
				break;

			case 11:
				System.out.println("Introduzca el nombre del planeta para eliminarlo");
				in.nextLine();
				nombre = in.nextLine();
				planeta.elminarPlaneta(nombre);
				break;
			case 12:
				System.out.println("Introduce el id del primer sistema solar");
				id = in.nextInt();
				System.out.println("Introduce el id del segundo sistema solar");
				idSistemaSolar = in.nextInt();
				sistemaSolar.intercambiarIdSistemasSolares(id, idSistemaSolar);
				sistemaSolar.listarSistemaSolar();
				break;
			case 13:
				System.out.println("Introduce el nombre de la estrella que desas renombrar");
				in.nextLine();
				estrella = in.nextLine();
				sistemaSolar.modificarNombreEstrella(estrella);
				sistemaSolar.listarSistemaSolar();
				break;
			case 14:
				System.out.println("Adios");
				break;
				default:
					System.out.println("Opcion no contemplada");
			}

		} while (opcion != 14);

		in.close();

	}

}